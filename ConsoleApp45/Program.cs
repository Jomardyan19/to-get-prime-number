﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeNumber
{
    class Program
    {
        static void Main(string[] args)
        {

            int count = 15;

            int[] arr = new int[count];
            Random rnd = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(50);

                int a = arr[i];
                {
                    bool isPrime = true;

                    if (a <= 1)
                        isPrime = false;

                    else if (a % 2 == 0 && a != 2)
                        isPrime = false;

                    else
                    {
                        double num = Math.Sqrt(a);
                        for (int div = 3; div <= num; div += 2)
                        {
                            if (a % div == 0)
                            {
                                isPrime = false;
                                break;
                            }
                        }

                    }
                    if (isPrime)
                        Console.WriteLine(a);
                }
            }

        }
    }
}
